#!/usr/bin/env node
process.argv.push('--cwd')
process.argv.push(process.cwd())
process.argv.push('--gulpfile')
process.argv.push(require.resolve('..')) // 其实找的就是这个，因为package指定的main字段 ../lib/index.js

require('gulp/bin/gulp')