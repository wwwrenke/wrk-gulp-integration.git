/**
 * gulp 入口文件
 */
const { src, dest, parallel, series, watch } = require('gulp');
const del = require('del')
const browserSync = require('browser-sync')
const bs = browserSync.create()  // 自动创建一个开发服务器
// const loadPlugins = require('gulp-load-plugins')
// const plugins = loadPlugins()
// const sass = require('gulp-sass')  // 转换scss文件
const sass = require('gulp-sass')(require('sass')); // 转换scss文件
const babel = require('gulp-babel')
const swig = require('gulp-swig')
const imagemin = require('gulp-imagemin')
const userefs = require('gulp-useref')
const gulp_if = require('gulp-if')
const htmlmin = require('gulp-htmlmin')
const uglify = require('gulp-uglify')
const clean_css = require('gulp-clean-css')
const cwd = process.cwd(); // 返回当前命令行所在的工作目录
let config = {
  // default config
  build: {
    src: 'src',
    dist: 'dist',
    temp: 'temp',
    public: 'public',
    paths: {
      styles: 'assets/styles/*.scss',
      scripts: 'assets/scripts/*.js',
      pages: '*.html',
      images: 'assets/images/**',
      fonts: 'assets/fonts/**'
    }
  }
}
try {
  const loadConfig = require(`${cwd}/pages.config.js`)
  config = Object.assign({}, config, loadConfig);
} catch (e) {}

// gulp案例 - 样式编译
const style = () => {
    // base 为了把文件写入到dist时，保留原本的路径
    // outputStyle 是将编译后的css 按照完全展开的方式展开
    return src(config.build.paths.styles, { base: config.build.src, cwd: config.build.src })
        .pipe(sass({ outputStyle: 'expanded' }))
        .pipe(dest(config.build.temp))
        .pipe(bs.reload({ stream: true }))  // 加上这个之后 下面的serve的files就不需要了
}
// gulp案例 - 脚本编译
const scripts = () => {
    return src(config.build.paths.scripts, { base: config.build.src, cwd: config.build.src })
        .pipe(babel({ presets: [require('@babel/preset-env')] })) // 转换es6语法  presets属性必填
        .pipe(dest(config.build.temp))
        .pipe(bs.reload({ stream: true }))
}
// gulp案例 - 页面模板编译
const page = () => {
    return src(config.build.paths.pages, { base: config.build.src, cwd: config.build.src })
        .pipe(swig({ data: config.data, defaults: { cache: false }}))
        .pipe(dest(config.build.temp))
        .pipe(bs.reload({ stream: true }))
}
// gulp案例 - 图片和字体文件的转换
const image = () => {
    return src(config.build.paths.images, {base: config.build.src, cwd: config.build.src})
        .pipe(imagemin())
        .pipe(dest(config.build.dist))
}
const font = () => {
    return src(config.build.paths.fonts, {base: config.build.src, cwd: config.build.src})
        .pipe(imagemin())
        .pipe(dest(config.build.dist))
}
// 其他文件转换 及文件清除
const extra = () => {
    return src('**', { base: config.build.public, cwd: config.build.public })
        .pipe(dest(config.build.dist))
}
const clean = () => {
    return del([config.build.dist, config.build.temp])
}

// gulp 热更新开发服务器(自动编译、自动更新浏览器页面)
const serve = () => {
    // gulp 监视变化以及构建过程优化(如何在src下的源代码修改过后，自动热更新)
    // 使用gulp的一个方法  watch监听各个编译任务，如果有修改，那么就自动重新更新该任务
    watch(config.build.paths.styles, { cwd: config.build.src }, style)
    watch(config.build.paths.scripts, { cwd: config.build.src }, scripts)
    watch(config.build.paths.pages, { cwd: config.build.src }, page)
    
    watch([
        config.build.paths.images,
        config.build.paths.fonts,
    ], {cwd: config.build.src}, bs.reload)

    watch(['**'], { cwd: config.build.public }, bs.reload)

    bs.init({
        notify: false, // 是否打开窗口的小提示
        port: 2080,  // 监听端口
        // open: false, // 是否自动打开浏览器
        // files: "dist/**",  // 监听哪些文件 修改后自动更新
        server: {  // 使用哪些文件夹作为项目目录
            baseDir: [config.build.temp, config.build.src, config.build.public],
            // 优先于 baseDir
            routes: {
                "/node_modules": "node_modules"
            }
        }
    })
}
// gulp useref文件引用处理 及 文件压缩 （分别压缩html、css、js）
const useref = () => {
    return src(config.build.paths.pages, {base: config.build.temp, cwd: config.build.temp})
        .pipe(userefs({ searchPath: [config.build.temp, '.'] }))
        // html js css
        .pipe(gulp_if(/\.js$/, uglify()))
        .pipe(gulp_if(/\.css$/, clean_css()))
        .pipe(gulp_if(/\.html$/, htmlmin({ collapseWhitespace: true, minifyCSS: true, minifyJS: true })))
        .pipe(dest(config.build.dist))
}

// 组合任务
const compile = parallel(style, scripts, page)

// 上线前执行的任务
const build = series(
    clean, 
    parallel(
        series(compile, useref), 
        extra, 
        image, 
        font
    )
) 

const develop = series(compile, serve)

module.exports = {
    build,
    clean,
    develop
}



















