# 封装自己的gulp集成  如果碰到多个相同性质的项目  为了多项目复用
# 创建一个模块，发布到npm仓库  --- 托管到github或码云

-------工作流准备 start -----------------------------------------------------------------------
# 创建了一个wrk-gulp-integration 放在了码云。
# 将初始化文件都弄好，提交到码云，因为这个wrk-gulp-integration是要放在npm仓库，
# 但是在开发阶段，这个模块并没有开发好,所以，使用yarn link的方式，将这个模块link到全局，
# 然后再去项目当中使用yarn link "wrk-gulp-integration" 就会自动的出现在我们项目的node_modules中
-------工作流准备 end -----------------------------------------------------------------------

-------提取gulpfile.js start -----------------------------------------------------------------------
# 将我们之前在项目中封装好的gulpfile复制到wrk-gulp-integration插件的lib/index入口文件，将作为项目中下载了这个# 插件的时候，用到的打包配置文件

# 将插件link到项目中后，直接运行yarn build会报出找不到gulp的错误，本地先安装上
# yarn add gulp --dev  这个问题等发布到npm仓库就不会出现了，因为会自动安装

# 然后再运行，还会报出一个调用文件的错误，因为插件当中的index引用了package.json文件，它指向的是插件中的，而# 不是我们项目中的，这个需要解决
-------提取gulpfile.js end -----------------------------------------------------------------------

-------解决模块中的问题 start -----------------------------------------------------------------------
# 将不需要提取的抽离出来，使用约定大于配置的方式，在我们本地项目中去自行配置，而不在插件中去配置
# 比如我们约定，让插件去读取项目中的特定的gulp外在配置文件，来获取一些东西，比如就叫pages.config.js
# 如何让插件读取本地项目中的文件，
const cwd = process.cwd(); // 返回当前命令行所在的工作目录
let config = {
  // default config
}
try {
  const loadConfig = require(`${cwd/pages.config.js}`)
  config = Object.assign({}, config, loadConfig);
} catch (e) {}
# 这样的话，我们就可以使用config.data来替换插件自己的调用

# 还会遇到的错误就是，在公共插件中引用的presets: ['@babel/preset-env'] 类似这种的，也会报错，因为我们
# 本地的项目中并没有下载这种的模块，就会报错，最好的方法就是，让插件自己引用自己的东西
# 改为 presets: [require('@babel/preset-env')]
3
-------解决模块中的问题 end -----------------------------------------------------------------------

-------抽象路径配置 start -----------------------------------------------------------------------
# 可以将插件内的定死的路径，比如 "src/assets/styles/*.scss"  "src/assets/scripts/*.js" 这种的，从使用者
# 的角度出发，并不是太理想，最好的是，这些也可以自定义配置，因为不是每个项目都跟我们的插件路径是一样的
# 然后就是将插件中的所有固定的路径，都改成配置中的路径，我们可以在pages.config.js中修改一下dist和temp路径试试
-------抽象路径配置 end -----------------------------------------------------------------------

-------包装gulp cli start -----------------------------------------------------------------------
# 还可以将这个模块变得更加便捷，上面的弄完了之后，还需要在项目中添加gulpfile和pages.config.js 
# 还能继续优化，让这两个文件都不需要创建就能用

# 首先删除项目中的gulpfile文件，运行yarn gulp 会报出找不到gulpfile文件的错误，
# 解决方法就是，可以通过命令指定gulpfile文件的路径
# yarn gulp --gulpfile build ./node_modules/wrk-gulp-integration/lib/index.js --cwd .

# 上面的命令太长且复杂，可以将wrk-gulp-integration封装成cli，指定cli入口，然后在入口里去调用gulp
# 在wrk-gulp-integration创建bin/wrk-gulp-integration.js
#!/usr/bin/env node
process.argv.push('--cwd')
process.argv.push(process.cwd())
process.argv.push('--gulpfile')
process.argv.push(require.resolve('..')) // 其实找的就是这个，因为package指定的main字段 ../lib/index.js

require('gulp/bin/gulp') // 执行gulp

# 然后将link先关掉  yarn unlink  再重启 yarn link 这样就可以在项目中使用wrk-gulp-integration build打包了
-------包装gulp cli end -----------------------------------------------------------------------

-------发布并使用 cli start -----------------------------------------------------------------------
# 发布前有个小问题，就是package.json中的files 需要把bin和lib都加上，这个发布需要的文件

# yarn publish --registry https://registry.yarnpkg.com
# 这里需要注意的问题，就是如果没有npm的账号，需要注册一个，发布的时候，需要填写这些
-------发布并使用 cli end -----------------------------------------------------------------------
